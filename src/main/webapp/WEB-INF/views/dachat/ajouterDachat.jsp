<%@ include file="/WEB-INF/views/includes/includes.jsp"%>
<!DOCTYPE html>
<html lang="fr">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Agility Formulaire Nouvelle Demande</title>


<!-- Bootstrap Core CSS -->
<link
	href="<%=request.getContextPath()%>/resources/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">

<!-- MetisMenu CSS -->
<link
	href="<%=request.getContextPath()%>/resources/vendor/metisMenu/metisMenu.min.css"
	rel="stylesheet">

<!-- Custom CSS -->
<link
	href="<%=request.getContextPath()%>/resources/dist/css/sb-admin-2.css"
	rel="stylesheet">

<!-- Custom Fonts -->
<link
	href="<%=request.getContextPath()%>/resources/vendor/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">
<!-- DataTables CSS -->
<link
	href="<%=request.getContextPath()%>/resources/vendor/datatables-plugins/dataTables.bootstrap.css"
	rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link
	href="<%=request.getContextPath()%>/resources/vendor/datatables-responsive/dataTables.responsive.css"
	rel="stylesheet">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

	<div id="wrapper">

		<!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation"
			style="margin-bottom: 0">

			<%@ include file="/WEB-INF/views/menutop/top_menu.jsp"%>
			<%@ include file="/WEB-INF/views/menuleft/left_menu.jsp"%>
		</nav>

		<!-- Page Content -->
		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">Formulaire Nouvelle demande d'achat</h1>
					</div>
					<!-- /.col-lg-12 -->
				</div>

				<div class="row">
					<div class="col-lg-4">
						<div class="panel panel-primary">

							<div class="panel-heading">
								<fmt:message key="dachat.nouveau" />
							</div>
							<!-- /.panel-heading -->
							<div class="panel-body">
								<c:url value="/dachat/nouveau" var="urlEnregistrer"></c:url>
								<f:form modelAttribute="dachat" action="${urlEnregistrer}"
									method="post">
									<div class="form-group">

										<f:hidden path="idDachat" />
										<label> <fmt:message key="dachat.date"></fmt:message>
										</label>
										<f:input path="dateDachat" class="form-control"
											placeholder="Date" />

										<label> <fmt:message key="dachat.objet"></fmt:message>
										</label>
										<f:input id="objetAch" path="objetDachat" class="form-control"
											placeholder="Objet" />

										<label> <fmt:message key="dachat.type"></fmt:message>
										</label>
										<f:input path="typeDachat" class="form-control"
											placeholder="Type" />

										<label> <fmt:message key="dachat.urgence"></fmt:message>
										</label>
										<f:input id="urgenceAch" path="urgenceDachat"
											class="form-control" placeholder="Urgence" />

										<label> <fmt:message key="dachat.justif"></fmt:message>
										</label>
										<f:input path="justBesoinDachat" class="form-control"
											placeholder="Justification" />

										<label> <fmt:message key="dachat.contact"></fmt:message>
										</label>
										<f:input path="contactLivDachat" class="form-control"
											placeholder="Contact de livraison" />

										<label> <fmt:message key="dachat.adresse"></fmt:message>
										</label>
										<f:input path="adrtLivDachat" class="form-control"
											placeholder="Adresse de livraison" />

										<label> <fmt:message key="common.fournisseur"></fmt:message>
										</label> <select class="form-control" id="listfournisseur">
											<c:forEach items="${fournisseurs}" var="frn">
												<option  value="${frn.getIdFournisseur() }">
													${frn.getNomFournisseur()}</option>
											</c:forEach>
										</select>
									</div>
									<div class="panel-footer">
										<button type="submit" class="btn btn-default">
											<i class="fa fa-save">&nbsp;&nbsp;<fmt:message
													key="common.enregistrer"></fmt:message></i>
										</button>
										<a href="<c:url value="/dachat/" > </c:url>"
											class="btn btn-danger"><i class="fa fa-arrow-left">&nbsp;&nbsp;<fmt:message
													key="common.annuler"></fmt:message></i></a>
									</div>

								</f:form>
							</div>
							<!-- /.panel-body -->


						</div>
						<!-- /.panel -->
					</div>
					<!-- /.col-lg-12 -->

					<div class="col-lg-6">
						<div class="panel panel-primary">
							<div class="panel-heading">Details de la demande</div>
							<!-- /.panel-heading -->
							<div class="panel-body">
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th>Article</th>
												<th>Quantit�</th>
												<th>Prix Unitaire</th>
												<th>Total</th>
												<th>Actions</th>
											</tr>
										</thead>

										<tbody id="detailNouvelleDachatl">
											<tr></tr>
										</tbody>
									</table>
								</div>
								<!-- /.table-responsive -->
							</div>
							<!-- /.panel-body -->
						</div>
						<!-- /.panel -->
					</div>
				</div>

			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- /#page-wrapper -->
	</div>
	<!-- /#wrapper -->
	<!-- jQuery -->
	<script
		src="<%=request.getContextPath()%>/resources/vendor/jquery/jquery.min.js"></script>
	<!-- Bootstrap Core JavaScript -->
	<script
		src="<%=request.getContextPath()%>/resources/vendor/bootstrap/js/bootstrap.min.js"></script>
	<!-- Metis Menu Plugin JavaScript -->
	<script
		src="<%=request.getContextPath()%>/resources/vendor/metisMenu/metisMenu.min.js"></script>
	<!-- Custom Theme JavaScript -->
	<script
		src="<%=request.getContextPath()%>/resources/dist/js/sb-admin-2.js"></script>
	<!-- DataTables JavaScript -->
	<script
		src="<%=request.getContextPath()%>/resources/vendor/datatables/js/jquery.dataTables.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendor/datatables-responsive/dataTables.responsive.js"></script>
	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
	<script>
		$(document).ready(function() {
			$('#dataTables-example').DataTable({
				responsive : true
			});
		});
	</script>

	<!-- Client JavaScript fichiers-->
	<script
		src="<%=request.getContextPath()%>/resources/javascript/dachat.js"></script>
</body>

</html>

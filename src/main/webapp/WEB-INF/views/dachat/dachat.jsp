<%@ include file="/WEB-INF/views/includes/includes.jsp"%>
<!DOCTYPE html>
<html lang="fr">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Agility Liste des demandes D'achat</title>


<!-- Bootstrap Core CSS -->
<link
	href="<%=request.getContextPath()%>/resources/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">

<!-- MetisMenu CSS -->
<link
	href="<%=request.getContextPath()%>/resources/vendor/metisMenu/metisMenu.min.css"
	rel="stylesheet">

<!-- Custom CSS -->
<link
	href="<%=request.getContextPath()%>/resources/dist/css/sb-admin-2.css"
	rel="stylesheet">

<!-- Custom Fonts -->
<link
	href="<%=request.getContextPath()%>/resources/vendor/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">
<!-- DataTables CSS -->
<link
	href="<%=request.getContextPath()%>/resources/vendor/datatables-plugins/dataTables.bootstrap.css"
	rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link
	href="<%=request.getContextPath()%>/resources/vendor/datatables-responsive/dataTables.responsive.css"
	rel="stylesheet">

</head>

<body>

	<div id="wrapper">

		<!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation"
			style="margin-bottom: 0">

			<%@ include file="/WEB-INF/views/menutop/top_menu.jsp"%>
			<%@ include file="/WEB-INF/views/menuleft/left_menu.jsp"%>
		</nav>

		<!-- Page Content -->
		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">Demandes d'achat</h1>
					</div>
					<!-- /.col-lg-12 -->
				</div>

				<div class="row">
					<div class="col-lg-12">
						<ol class="breadcrumb">
							<li><a href="<c:url value="/dachat/nouveau" > </c:url>"><i
									class="fa fa-plus">&nbsp;<fmt:message key="common.ajouter" />
								</i> </a></li>
							<li><a href="#"><i class="glyphicon glyphicon-import"><fmt:message
											key="common.importer" /> </i> </a></li>
							<li><a href="#"><i class="glyphicon glyphicon-export"><fmt:message
											key="common.exporter" /> </i> </a></li>
						</ol>
					</div>
					<!-- /.col-lg-12 -->
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<fmt:message key="dachat.listeDa" />
							</div>
							<!-- /.panel-heading -->
							<div class="panel-body">
								<table width="100%"
									class="table table-striped table-bordered table-hover"
									id="dataTables-example">
									<thead>
										<tr>

											<th><fmt:message key="dachat.date" /></th>
											<th><fmt:message key="dachat.objet" /></th>
											<th><fmt:message key="dachat.type" /></th>
											<th><fmt:message key="dachat.urgence" /></th>
											<th><fmt:message key="dachat.justif" /></th>
											<th><fmt:message key="dachat.adresse" /></th>
											<th><fmt:message key="dachat.total" /></th>
											<th><fmt:message key="common.action" /></th>
										</tr>
									</thead>
									<tbody>

										<c:forEach items="${dachats }" var="dachat">
											<tr class="odd gradeA">
												<td class="center">${dachat.getDateDachat()}</td>
												<td class="center">${dachat.getObjetDachat()}</td>
												<td class="center">${dachat.getTypeDachat()}</td>
												<td class="center">${dachat.getUrgenceDachat()}</td>
												<td class="center">${dachat.getJustBesoinDachat()}</td>
												<td class="center">${dachat.getAdrtLivDachat()}</td>
												<td class="center">${dachat.getTotalHtDa()}</td>

												<td class="center"><textarea style="display: none;"
														id="json${dachat.getIdDachat()}"> ${dachat.getLdachatsJson()}  </textarea>
													<button
														onclick="updateDetailDemande(${dachat.getIdDachat()});">
														<i class="fa fa-list"> </i>
													</button> &nbsp;|&nbsp; <c:url
														value="/dachat/modifier/${dachat.getIdDachat()}"
														var="urlModifier">
													</c:url> <a href="${urlModifier}"><i class="fa fa-edit">&nbsp;
													</i> </a> &nbsp;|&nbsp; <a href="javascript:void(0);"
													data-toggle="modal"
													data-target="#modalDachat${dachat.getIdDachat()}"><i
														class="fa fa-trash-o"></i> </a> &nbsp;|&nbsp; <c:url
														value="/dachat/print/${dachat.getIdDachat()}"
														var="urlPrint">
													</c:url> <a id="printbut" href="${urlPrint}"><i
														class="glyphicon glyphicon-print">&nbsp; </i> </a>

													<div class="modal fade"
														id="modalDachat${dachat.getIdDachat()}" tabindex="-1"
														role="dialog" aria-labelledby="myModalLabel"
														aria-hidden="true">
														<div class="modal-dialog">
															<div class="modal-content">
																<div class="modal-header">
																	<button type="button" class="close"
																		data-dismiss="modal" aria-hidden="true">&times;</button>
																	<h4 class="modal-title" id="myModalLabel">
																		<fmt:message key="common.confirmer.suppression" />
																	</h4>
																</div>
																<div class="modal-body">
																	<fmt:message key="dachat.confirmer.supression" />${dachat.getIdDachat()}
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-primary"
																		data-dismiss="modal">
																		<fmt:message key="common.annuler" />
																	</button>
																	<c:url
																		value="/dachat/supprimer/${dachat.getIdDachat()}"
																		var="urlSupprimer"></c:url>
																	<a href="${urlSupprimer}" class="btn btn-danger"> <i
																		class="fa fa-trash-o"></i>&nbsp;&nbsp; <fmt:message
																			key="common.confirmer" />
																	</a>
																</div>
															</div>
															<!-- /.modal-content -->
														</div>
														<!-- /.modal-dialog -->
													</div></td>
											</tr>
										</c:forEach>

									</tbody>
								</table>
								<!-- /.table-responsive -->

							</div>
							<!-- /.panel-body -->
						</div>
						<!-- /.panel -->
					</div>
					<!-- /.col-lg-12 -->
				</div>
				<div class="col-lg-6">
					<div class="panel panel-default">
						<div class="panel-heading">Details Demande d'achat</div>
						<!-- /.panel-heading -->
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Article</th>
											<th>Quantit�</th>
											<th>Prix Unitaire</th>
											<th>Total</th>
											<th>Actions</th>
										</tr>
									</thead>

									<tbody id="detailDachatl">

									</tbody>
								</table>
							</div>
							<!-- /.table-responsive -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>

			</div>
			<!-- /.container-fluid -->





		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->

	<!-- jQuery -->
	<script
		src="<%=request.getContextPath()%>/resources/vendor/jquery/jquery.min.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script
		src="<%=request.getContextPath()%>/resources/vendor/bootstrap/js/bootstrap.min.js"></script>

	<!-- Metis Menu Plugin JavaScript -->
	<script
		src="<%=request.getContextPath()%>/resources/vendor/metisMenu/metisMenu.min.js"></script>

	<!-- Custom Theme JavaScript -->
	<script
		src="<%=request.getContextPath()%>/resources/dist/js/sb-admin-2.js"></script>

	<!-- DataTables JavaScript -->
	<script
		src="<%=request.getContextPath()%>/resources/vendor/datatables/js/jquery.dataTables.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/vendor/datatables-responsive/dataTables.responsive.js"></script>
	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
	<script>
		$(document).ready(function() {
			$('#dataTables-example').DataTable({
				responsive : true
			});
		});
	</script>

	<!-- Client JavaScript fichiers-->
	<script
		src="<%=request.getContextPath()%>/resources/javascript/demandeachat.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/javascript/PrintRep.js"></script>

</body>

</html>

<%@ include file="/WEB-INF/views/includes/includes.jsp"%>
<!DOCTYPE html>
<html lang="fr">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Home Agility</title>

<!-- Bootstrap Core CSS -->
<link
	href="<%=request.getContextPath()%>/resources/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">

<!-- MetisMenu CSS -->
<link
	href="<%=request.getContextPath()%>/resources/vendor/metisMenu/metisMenu.min.css"
	rel="stylesheet">

<!-- Custom CSS -->
<link
	href="<%=request.getContextPath()%>/resources/dist/css/sb-admin-2.css"
	rel="stylesheet">

<!-- Custom Fonts -->
<link
	href="<%=request.getContextPath()%>/resources/vendor/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

	<div id="wrapper">

		<!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation"
			style="margin-bottom: 0">

			<%@ include file="/WEB-INF/views/menutop/top_menu.jsp"%>
			<%@ include file="/WEB-INF/views/menuleft/left_menu.jsp"%>
		</nav>

		<!-- Page Content -->
		<div id="page-wrapper">
			<div class="container-fluid">
			
			


				<div class="panel panel-default">
					<div class="panel-heading">
						<i class="fa fa-clock-o fa-fw"></i> WORKFLOW ACHAT
					</div>
					<!-- /.panel-heading -->
					<div class="panel-body">
						<ul class="timeline">
							<li>
								<div class="timeline-badge">
									<i class="fa fa-check"></i>
								</div>
								<div class="timeline-panel">
									<div class="timeline-heading">
										<h4 class="timeline-title">Lorem ipsum dolor</h4>
										<p>
											<small class="text-muted"><i class="fa fa-clock-o"></i>
												11 hours ago via Twitter</small>
										</p>
									</div>
									<div class="timeline-body">
										<p>Algerian motors services Mercedes-Benz SPA AMS-MB
											annonce avoir obtenu les autorisations nécessaires pour
											démarrer ses opérations.</p>
									</div>
								</div>
							</li>
							<li class="timeline-inverted">
								<div class="timeline-badge warning">
									<i class="fa fa-credit-card"></i>
								</div>
								<div class="timeline-panel">
									<div class="timeline-heading">
										<h4 class="timeline-title">Lorem ipsum dolor</h4>
									</div>
									<div class="timeline-body">
										<p>Algerian motors services Mercedes-Benz SPA AMS-MB
											annonce avoir obtenu les autorisations nécessaires pour
											démarrer ses opérations.</p>
									</div>
								</div>
							</li>
							<li>
								<div class="timeline-badge danger">
									<i class="fa fa-bomb"></i>
								</div>
								<div class="timeline-panel">
									<div class="timeline-heading">
										<h4 class="timeline-title">Lorem ipsum dolor</h4>
									</div>
									<div class="timeline-body">
										<p>Algerian motors services Mercedes-Benz SPA AMS-MB
											annonce avoir obtenu les autorisations nécessaires pour
											démarrer ses opérations.</p>
									</div>
								</div>
							</li>
							<li class="timeline-inverted">
								<div class="timeline-panel">
									<div class="timeline-heading">
										<h4 class="timeline-title">Lorem ipsum dolor</h4>
									</div>
									<div class="timeline-body">
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing
											elit. Voluptates est quaerat asperiores sapiente, eligendi,
											nihil. Itaque quos, alias sapiente rerum quas odit! Aperiam
											officiis quidem delectus libero, omnis ut debitis!</p>
									</div>
								</div>
							</li>
							<li>
								<div class="timeline-badge info">
									<i class="fa fa-save"></i>
								</div>
								<div class="timeline-panel">
									<div class="timeline-heading">
										<h4 class="timeline-title">Lorem ipsum dolor</h4>
									</div>
									<div class="timeline-body">
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing
											elit. Nobis minus modi quam ipsum alias at est molestiae
											excepturi delectus nesciunt, quibusdam debitis amet, beatae
											consequuntur impedit nulla qui! Laborum, atque.</p>
										<hr>
										<div class="btn-group">
											<button type="button"
												class="btn btn-primary btn-sm dropdown-toggle"
												data-toggle="dropdown">
												<i class="fa fa-gear"></i> <span class="caret"></span>
											</button>
											<ul class="dropdown-menu" role="menu">
												<li><a href="#">Action</a></li>
												<li><a href="#">Another action</a></li>
												<li><a href="#">Something else here</a></li>
												<li class="divider"></li>
												<li><a href="#">Separated link</a></li>
											</ul>
										</div>
									</div>
								</div>
							</li>
							<li>
								<div class="timeline-panel">
									<div class="timeline-heading">
										<h4 class="timeline-title">Lorem ipsum dolor</h4>
									</div>
									<div class="timeline-body">
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing
											elit. Sequi fuga odio quibusdam. Iure expedita, incidunt unde
											quis nam! Quod, quisquam. Officia quam qui adipisci quas
											consequuntur nostrum sequi. Consequuntur, commodi.</p>
									</div>
								</div>
							</li>
							<li class="timeline-inverted">
								<div class="timeline-badge success">
									<i class="fa fa-graduation-cap"></i>
								</div>
								<div class="timeline-panel">
									<div class="timeline-heading">
										<h4 class="timeline-title">Lorem ipsum dolor</h4>
									</div>
									<div class="timeline-body">
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing
											elit. Deserunt obcaecati, quaerat tempore officia voluptas
											debitis consectetur culpa amet, accusamus dolorum fugiat,
											animi dicta aperiam, enim incidunt quisquam maxime neque
											eaque.</p>
									</div>
								</div>
							</li>
						</ul>
					</div>
					<!-- /.panel-body -->
				</div>
				<!-- /.panel -->






			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->

	<!-- jQuery -->
	<script
		src="<%=request.getContextPath()%>/resources/vendor/jquery/jquery.min.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script
		src="<%=request.getContextPath()%>/resources/vendor/bootstrap/js/bootstrap.min.js"></script>

	<!-- Metis Menu Plugin JavaScript -->
	<script
		src="<%=request.getContextPath()%>/resources/vendor/metisMenu/metisMenu.min.js"></script>

	<!-- Custom Theme JavaScript -->
	<script
		src="<%=request.getContextPath()%>/resources/dist/js/sb-admin-2.js"></script>

</body>

</html>

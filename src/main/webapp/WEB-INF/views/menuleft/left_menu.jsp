
<div class="navbar-default sidebar" role="navigation">
	<div class="sidebar-nav navbar-collapse">
		<ul class="nav" id="side-menu">
			<li class="sidebar-search">
				<div class="input-group custom-search-form">
					<input type="text" class="form-control" placeholder="Search...">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button">
							<i class="fa fa-search"></i>
						</button>
					</span>
				</div> <!-- /input-group -->
			</li>
			<c:url value="/" var="home" />
			<li><a href="${home}"><i class="fa fa-dashboard fa-fw"></i>
					<fmt:message key="common.dashbord" /></a></li>


			<li><a href="#"><i class="fa fa-group"></i> <fmt:message
						key="dachat.da" /></a>
				<ul class="nav nav-second-level">
					<c:url value="/dachat/nouveau" var="ajouterDa" />
					<li><a href="${ajouterDa}"><i
							class="fa fa-plus-square fa-fw"></i>
						<fmt:message key="dachat.ajouterDa" /> </a></li>
					<c:url value="/dachat/" var="listeDa" />
					<li><a href="${listeDa}"><i
							class="fa fa-plus-square fa-fw"></i>
						<fmt:message key="dachat.listeDa" /> </a></li>

				</ul> <!-- /.nav-second-level --></li>



			<li><a href="#"><i class="fa fa-wrench fa-fw"></i> <fmt:message
						key="common.parametrage" /><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level">

					<li><a href="panels-wells.html"><fmt:message
								key="common.parametrage.utilisateur" /></a></li>
					<c:url value="/fournisseur/nouveau" var="ajouterFou" />
					<li><a href="${ajouterFou}"><fmt:message
								key="common.parametrage.fournisseur" /></a></li>
				</ul> <!-- /.nav-second-level --></li>

		</ul>
	</div>

	<!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->
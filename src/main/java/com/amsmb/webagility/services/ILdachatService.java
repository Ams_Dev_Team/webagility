package com.amsmb.webagility.services;

import java.util.List;

import com.amsmb.webagility.entities.Ldachat;

public interface ILdachatService {

	public Ldachat save(Ldachat entity);

	public Ldachat update(Ldachat entity);

	public List<Ldachat> selectAll();

	public Ldachat getById(Long id);

	public void remove(Long id);

	public List<Ldachat> selectAll(String sortField, String sort);

	public Ldachat findOne(String paramName, Object paramValue);

	public Ldachat findOne(String[] paramName, Object[] paramValue);

	public int findCountBy(String paramName, String paramValue);

	public List<Ldachat> getByIdDachat(Long idDachat);

}

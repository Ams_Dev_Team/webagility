package com.amsmb.webagility.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.amsmb.webagility.dao.ILdachatDao;
import com.amsmb.webagility.entities.Ldachat;
import com.amsmb.webagility.services.ILdachatService;

 @Transactional
public class LdachatServiceImpl implements ILdachatService{

	private ILdachatDao dao;

	public void setDao(ILdachatDao dao) {
		this.dao = dao;
	}

	@Override
	public Ldachat save(Ldachat entity) {
		// TODO Auto-generated method stub
		return dao.save(entity);
	}

	@Override
	public Ldachat update(Ldachat entity) {
		// TODO Auto-generated method stub
		return dao.update(entity);
	}

	@Override
	public List<Ldachat> selectAll() {
		// TODO Auto-generated method stub
		return dao.selectAll();
	}

	@Override
	public Ldachat getById(Long id) {
		// TODO Auto-generated method stub
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		// TODO Auto-generated method stub
		dao.remove(id);
	}

	@Override
	public List<Ldachat> selectAll(String sortField, String sort) {
		// TODO Auto-generated method stub
		return dao.selectAll(sortField, sort);
	}

	@Override
	public Ldachat findOne(String paramName, Object paramValue) {
		// TODO Auto-generated method stub
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public Ldachat findOne(String[] paramName, Object[] paramValue) {
		// TODO Auto-generated method stub
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		// TODO Auto-generated method stub
		return dao.findCountBy(paramName, paramValue);
	}

	@Override
	public List<Ldachat> getByIdDachat(Long idDachat) {
		// TODO Auto-generated method stub
		return dao.getByIdDachat(idDachat);
	}

}

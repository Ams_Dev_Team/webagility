package com.amsmb.webagility.services.impl;

import java.io.InputStream;

import com.amsmb.webagility.dao.IFlickrDao;
import com.amsmb.webagility.services.IFlickrService;

public class FlickrServiceImpl implements IFlickrService {

	private IFlickrDao dao;
	
	public void setDao(IFlickrDao dao) {
		this.dao = dao;
	}
	@Override
	public String savePhoto(InputStream stream, String fileName) throws Exception {
		return dao.savePhoto(stream, fileName);
	}

}

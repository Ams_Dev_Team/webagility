package com.amsmb.webagility.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.amsmb.webagility.dao.IDachatDao;
import com.amsmb.webagility.entities.Dachat;
import com.amsmb.webagility.services.IDachatService;

 @Transactional
public class DachatServiceImpl implements IDachatService{

	private IDachatDao dao;

	public void setDao(IDachatDao dao) {
		this.dao = dao;
	}

	@Override
	public Dachat save(Dachat entity) {
		// TODO Auto-generated method stub
		return dao.save(entity);
	}

	@Override
	public Dachat update(Dachat entity) {
		// TODO Auto-generated method stub
		return dao.update(entity);
	}

	@Override
	public List<Dachat> selectAll() {
		// TODO Auto-generated method stub
		return dao.selectAll();
	}

	@Override
	public Dachat getById(Long id) {
		// TODO Auto-generated method stub
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		// TODO Auto-generated method stub
		dao.remove(id);
	}

	@Override
	public List<Dachat> selectAll(String sortField, String sort) {
		// TODO Auto-generated method stub
		return dao.selectAll(sortField, sort);
	}

	@Override
	public Dachat findOne(String paramName, Object paramValue) {
		// TODO Auto-generated method stub
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public Dachat findOne(String[] paramName, Object[] paramValue) {
		// TODO Auto-generated method stub
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		// TODO Auto-generated method stub
		return dao.findCountBy(paramName, paramValue);
	}

}

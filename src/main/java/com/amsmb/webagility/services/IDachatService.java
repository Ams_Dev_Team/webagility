package com.amsmb.webagility.services;

import java.util.List;

import com.amsmb.webagility.entities.Dachat;

public interface IDachatService {

	public Dachat save(Dachat entity);

	public Dachat update(Dachat entity);

	public List<Dachat> selectAll();

	public Dachat getById(Long id);

	public void remove(Long id);

	public List<Dachat> selectAll(String sortField, String sort);

	public Dachat findOne(String paramName, Object paramValue);

	public Dachat findOne(String[] paramName, Object[] paramValue);

	public int findCountBy(String paramName, String paramValue);

}

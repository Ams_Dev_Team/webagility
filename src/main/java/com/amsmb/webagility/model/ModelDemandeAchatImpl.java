package com.amsmb.webagility.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.amsmb.webagility.entities.Article;
import com.amsmb.webagility.entities.Dachat;
import com.amsmb.webagility.entities.Fournisseur;
import com.amsmb.webagility.entities.Ldachat;

@Component
public class ModelDemandeAchatImpl implements ModelDemandeAchat{

	
	private Dachat dachat ;
	private Map<Long, Ldachat> lignedachat = new HashMap<Long, Ldachat>();
	@Override
	public void creerDemande(Fournisseur fournisseur) {

		if (fournisseur==null)
		{
			return;
		}
		dachat=new Dachat();
		dachat.setFournisseur(fournisseur);
		dachat.setDateDachat(new Date());
		dachat.setIdDachat((long)1);
		
		
		
	}

	@Override
	public Ldachat ajouterLdachat(Article article) {
		if(article==null)
		{
		return null;
		}
		Ldachat lc = lignedachat.get(article.getIdArticle()) ;
		if(lc!=null )
		{
			int qte =lc.getQuanitedachat()+1;
			lc.setQuanitedachat(qte);
			lignedachat.put(article.getIdArticle(), lc);
		}else
		{
			lc.setDesignationarticle(article.getDesignationArticle());
			lc.setPrixht(article.getPrixHtArticle());
			lc.setQuanitedachat(1);
			lignedachat.put(article.getIdArticle(), lc);
			return lc;
		}
		
		return null;
	}

	@Override
	public Ldachat supprimerLdachat(Article article) {
		
		if (article==null)
			return null;
		
		Ldachat lc = lignedachat.get(article.getIdArticle()) ;
		return lignedachat.remove(article.getIdArticle());
	}

	@Override
	public Ldachat modifierLdachat(Article article, int qte) {
		
		if (article==null)
		{
			return null;
		}
		Ldachat lc = lignedachat.get(article.getIdArticle()) ;
		if (lc==null)
		{
			return null;
		}
		lc.setQuanitedachat(qte);
		
		return lignedachat.put(article.getIdArticle(), lc);
	}

	
	

}

package com.amsmb.webagility.model;

import com.amsmb.webagility.entities.Article;
import com.amsmb.webagility.entities.Fournisseur;
import com.amsmb.webagility.entities.Ldachat;

public interface ModelDemandeAchat {

	void creerDemande(Fournisseur fournisseur);
	Ldachat ajouterLdachat (Article article);
	Ldachat  supprimerLdachat (Article article);
	Ldachat modifierLdachat (Article article,int qte);
}

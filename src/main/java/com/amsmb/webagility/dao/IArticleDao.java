package com.amsmb.webagility.dao;

import com.amsmb.webagility.entities.Article;

public interface IArticleDao extends IGenericDao<Article>{

}

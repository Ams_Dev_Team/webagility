package com.amsmb.webagility.dao;

import java.util.List;

import com.amsmb.webagility.entities.Ldachat;

public interface ILdachatDao extends IGenericDao<Ldachat>{

	public List<Ldachat> getByIdDachat(Long idDachat);
}

package com.amsmb.webagility.dao;

import com.amsmb.webagility.entities.Fournisseur;

public interface IFournisseurDao extends IGenericDao<Fournisseur>{

}

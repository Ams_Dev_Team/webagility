package com.amsmb.webagility.dao.impl;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.amsmb.webagility.dao.IGenericDao;

@SuppressWarnings("unchecked")
public class GenericDaoImpl<E> implements IGenericDao<E> {

	@PersistenceContext
	EntityManager em;

	private Class<E> type;

	public Class<E> getType() {
		return type;
	}
	
	
	public GenericDaoImpl() {
		Type t = getClass().getGenericSuperclass();
		ParameterizedType pt = (ParameterizedType) t;
		type = (Class<E>) pt.getActualTypeArguments()[0];
	}

	
	@Override
	public E save(E entity) {

		em.persist(entity);
		return entity;
	}

	
	@Override
	public E update(E entity) {
		em.merge(entity); 	
		return entity;
	}



	@Override
	public List<E> selectAll() {
		Query query = em.createQuery("SELECT t FROM " + type.getSimpleName() + " t ");
		return query.getResultList();
	}

	@Override
	public List<E> selectAll(String sortField, String sort) {
		Query query = em.createQuery("SELECT t FROM " + type.getSimpleName() + " t ORDER BY " + sortField + " " + sort);
		return query.getResultList();
	}

	@Override
	public E getById(Long id) {

		return em.find(type, id);
	}

	@Override
	public void remove(Long id) {
		E tab = em.getReference( type, id);
		em.remove(tab);

	}

	@Override
	public E findOne(String paramName, Object paramValue) {
		Query query = em.createQuery("SELECT t FROM " + type.getSimpleName() + " t WHERE " + paramName + "= :x");
	 	query.setParameter(paramName, paramValue);
		return query.getResultList().size() > 0 ? (E) query.getResultList().get(0) : null;
	}

	@Override
	public E findOne(String[] paramName, Object[] paramValue) {
		if (paramName.length!= paramValue.length)
		{
			return null;
		}
		
		String queryString = "SELECT e FROM " + type.getSimpleName() + "e WHERE ";
		int len = paramName.length;
		for (int i = 0; i < len; i++) {
			queryString +=" e."+ paramName[i] +" =:x"+i;
			if(i<len-1)
			{
				queryString+=" and ";
			}			
		}
		Query query =em.createQuery(queryString);
		for(int i=0;i<paramValue.length;i++)
		{
			query.setParameter("x"+i, paramValue[i]);
		}
		
		return query.getResultList().size()>0 ? (E) query.getResultList().get(0) : null;
		
		
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		Query query = em.createQuery("SELECT t FROM " + type.getSimpleName() + " t WHERE " + paramName + "= :x");
	 	query.setParameter(paramName, paramValue);
		return query.getResultList().size() > 0 ? ((Long) query.getSingleResult()).intValue() : 0;
	
	}

}

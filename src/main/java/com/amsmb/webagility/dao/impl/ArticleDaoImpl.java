package com.amsmb.webagility.dao.impl;


import com.amsmb.webagility.dao.IArticleDao;
import com.amsmb.webagility.entities.Article;

public class ArticleDaoImpl extends GenericDaoImpl<Article> implements IArticleDao {

}

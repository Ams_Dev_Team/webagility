package com.amsmb.webagility.dao.impl;

import java.util.List;

import javax.persistence.Query;

import com.amsmb.webagility.dao.ILdachatDao;

import com.amsmb.webagility.entities.Ldachat;

public class LdachatDaoImpl extends GenericDaoImpl<Ldachat> implements ILdachatDao {

	@Override
	public List<Ldachat> getByIdDachat(Long idDachat) {
        String querySring ="SELECT Ld FROM "+ Ldachat.class.getSimpleName() +" ld WHERE ld.dachat.idDachat= :x";
        Query query =em.createQuery(querySring);
        query.setParameter("x", idDachat);
        
		return query.getResultList();
	}

}

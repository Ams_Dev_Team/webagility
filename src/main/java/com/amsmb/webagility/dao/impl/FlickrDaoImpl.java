package com.amsmb.webagility.dao.impl;

import java.io.InputStream;
import javax.swing.JOptionPane;
import org.scribe.model.Verifier;
import org.scribe.model.Token;
import com.flickr4java.flickr.Flickr;
import com.flickr4java.flickr.FlickrException;
import com.flickr4java.flickr.REST;
import com.flickr4java.flickr.RequestContext;
import com.flickr4java.flickr.auth.Auth;
import com.flickr4java.flickr.auth.AuthInterface;
import com.flickr4java.flickr.auth.Permission;
import com.flickr4java.flickr.uploader.UploadMetaData;
import com.amsmb.webagility.dao.IFlickrDao;

public class FlickrDaoImpl implements IFlickrDao {

	private Flickr flikr;

	private UploadMetaData uploadMetaData = new UploadMetaData();

	private String apiKey = "9397c9a8c32ed129768f10b574f06ec1";

	private String sharedSecret = "eeb08909f560f549";

	private void connect() {

		flikr = new Flickr(apiKey, sharedSecret, new REST());
		Auth auth = new Auth();
		auth.setPermission(Permission.READ);
		auth.setToken("72157705876129055-30f80613244d8c15");
		auth.setTokenSecret("f7c34a87cff49892");
		RequestContext requestContext = RequestContext.getRequestContext();
		requestContext.setAuth(auth);
		flikr.setAuth(auth);

	}

	@Override
	public String savePhoto(InputStream stream, String fileName) throws Exception {
		connect();
		uploadMetaData.setTitle(fileName);
		String photoId = flikr.getUploader().upload(stream, uploadMetaData);
		return flikr.getPhotosInterface().getPhoto(photoId).getMedium640Url();
	}

	public void auth() {
		// TODO Auto-generated method stub
		flikr = new Flickr(apiKey, sharedSecret, new REST());
		AuthInterface authInterface = flikr.getAuthInterface();

		Token token = authInterface.getRequestToken();
		System.out.println("token: " + token);
		String url = authInterface.getAuthorizationUrl(token, Permission.DELETE);
		System.out.println("Flow this url to autorise yourself in flickr");
		System.out.println(url);
		System.out.println("PAste in the token it give You");
		System.out.println(">>");

		String tokenKey = JOptionPane.showInputDialog(null);
		Token reqeustToken = authInterface.getAccessToken(token, new Verifier(tokenKey));
		System.out.println("Avec Succ�s ");

		Auth auth = null;
		try {
			auth = authInterface.checkToken(reqeustToken);
		} catch (FlickrException e) {
			e.printStackTrace();
		}
		System.out.println("token: " + reqeustToken.getToken());
		System.out.println("secret: " + reqeustToken.getSecret());
		System.out.println("id: " + auth.getUser().getId());
		System.out.println("RealName: " + auth.getUser().getRealName());
		System.out.println("Username: " + auth.getUser().getUsername());
		System.out.println("Permission: " + auth.getPermission().getType());

	}
}

package com.amsmb.webagility.entities;


import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;


@Entity
public class Article implements Serializable{


	@Id
	@GeneratedValue
	private Long idArticle;

	private String designationArticle;
	private double prixHtArticle;
	public Long getIdArticle() {
		return idArticle;
	}



	public void setIdArticle(Long idArticle) {
		this.idArticle = idArticle;
	}



	public String getDesignationArticle() {
		return designationArticle;
	}



	public void setDesignationArticle(String designationArticle) {
		this.designationArticle = designationArticle;
	}



	public double getPrixHtArticle() {
		return prixHtArticle;
	}



	public void setPrixHtArticle(double prixHtArticle) {
		this.prixHtArticle = prixHtArticle;
	}



	public String getCategoryArticle() {
		return categoryArticle;
	}



	public void setCategoryArticle(String categoryArticle) {
		this.categoryArticle = categoryArticle;
	}



	private String categoryArticle;
	
	@OneToMany(mappedBy="article")	
	private List <Ldachat> ldachats;


	
	public Article() {
	}

		
}


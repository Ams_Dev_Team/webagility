package com.amsmb.webagility.entities;


import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;


@Entity
public class Fournisseur implements Serializable{


	@Id
	@GeneratedValue
	private Long idFournisseur;

	private String nomFournisseur;
	private String numTelFournisseur;
	private String adrFournisseur;
	
	@OneToMany(mappedBy="fournisseur")	
	private List <Dachat> dachats;

	
	public Long getIdFournisseur() {
		return idFournisseur;
	}



	public void setIdFournisseur(Long idFournisseur) {
		this.idFournisseur = idFournisseur;
	}



	public String getNomFournisseur() {
		return nomFournisseur;
	}



	public void setNomFournisseur(String nomFournisseur) {
		this.nomFournisseur = nomFournisseur;
	}



	public String getNumTelFournisseur() {
		return numTelFournisseur;
	}



	public void setNumTelFournisseur(String numTelFournisseur) {
		this.numTelFournisseur = numTelFournisseur;
	}



	public String getAdrFournisseur() {
		return adrFournisseur;
	}



	public void setAdrFournisseur(String adrFournisseur) {
		this.adrFournisseur = adrFournisseur;
	}



	public Fournisseur() {
	}

		
}


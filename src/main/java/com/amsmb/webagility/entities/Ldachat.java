
package com.amsmb.webagility.entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
public class Ldachat implements Serializable {

	@Id
	@GeneratedValue
	private Long idLdachat;
	private String designationarticle;
	private int quanitedachat;
	private double prixht;

	@ManyToOne
	@JoinColumn(name = "idDachat")
	private Dachat dachat;

	@ManyToOne
	@JoinColumn(name = "idArticle")
	private Article article;

	
	public Long getIdLdachat() {
		return idLdachat;
	}

	public void setIdLdachat(Long idLdachat) {
		this.idLdachat = idLdachat;
	}

	public int getQuanitedachat() {
		return quanitedachat;
	}

	public void setQuanitedachat(int quanitedachat) {
		this.quanitedachat = quanitedachat;
	}

	public String getDesignationarticle() {
		return designationarticle;
	}

	public void setDesignationarticle(String designationarticle) {
		this.designationarticle = designationarticle;
	}

	public double getPrixht() {
		return prixht;
	}

	public void setPrixht(double prixht) {
		this.prixht = prixht;
	}

	


	public Ldachat() {
	}

}

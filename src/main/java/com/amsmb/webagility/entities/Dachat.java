package com.amsmb.webagility.entities;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
public class Dachat implements Serializable{


	@Id
	@GeneratedValue
	private Long idDachat;
	private Date dateDachat;
	private String objetDachat;
	private String typeDachat;	
	private String urgenceDachat;
	private String justBesoinDachat;
	private String contactLivDachat;
	private String adrtLivDachat;
	
	@OneToMany(mappedBy="dachat")	
	private List <Ldachat> ldachats;
	
	@ManyToOne
	@JoinColumn(name = "idFournisseur")
	private Fournisseur fournisseur;
	
	@Transient
	private String ldachatsJson;
	
	public Long getIdDachat() {
		return idDachat;
	}


	public void setIdDachat(Long idDachat) {
		this.idDachat = idDachat;
	}


	public Date getDateDachat() {
		return dateDachat;
	}


	public void setDateDachat(Date dateDachat) {
		this.dateDachat = dateDachat;
	}


	public String getObjetDachat() {
		return objetDachat;
	}


	public void setObjetDachat(String objetDachat) {
		this.objetDachat = objetDachat;
	}


	public String getTypeDachat() {
		return typeDachat;
	}


	public void setTypeDachat(String typeDachat) {
		this.typeDachat = typeDachat;
	}


	public String getUrgenceDachat() {
		return urgenceDachat;
	}


	public void setUrgenceDachat(String urgenceDachat) {
		this.urgenceDachat = urgenceDachat;
	}


	public String getJustBesoinDachat() {
		return justBesoinDachat;
	}


	public void setJustBesoinDachat(String justBesoinDachat) {
		this.justBesoinDachat = justBesoinDachat;
	}


	public Fournisseur getFournisseur() {
		return fournisseur;
	}


	public void setFournisseur(Fournisseur fournisseur) {
		this.fournisseur = fournisseur;
	}


	public String getContactLivDachat() {
		return contactLivDachat;
	}


	public void setContactLivDachat(String contactLivDachat) {
		this.contactLivDachat = contactLivDachat;
	}


	public String getAdrtLivDachat() {
		return adrtLivDachat;
	}


	public void setAdrtLivDachat(String adrtLivDachat) {
		this.adrtLivDachat = adrtLivDachat;
	}

	@JsonIgnore
	public List<Ldachat> getLdachats() {
		return ldachats;
	}


	public void setLdachats(List<Ldachat> ldachats) {
		this.ldachats = ldachats;
	}


	@JsonIgnore
	public double getTotalHtDa() {
		double prixtotal=0;
		if( !ldachats.isEmpty()) {
			
			for(Ldachat ldachat : ldachats)
			{
				prixtotal=prixtotal+ldachat.getPrixht();
			}
		}
		else
		{
			
		}
	return prixtotal;	
	}
	
 
 @JsonIgnore	
 public String getLdachatsJson() {
	 
	
		 if (! ldachats.isEmpty())
		 {		
			 try {
				 ObjectMapper mapper = new ObjectMapper();
				 return mapper.writeValueAsString(ldachats);
				 
			} catch (JsonGenerationException e) {
				
				e.printStackTrace();
			} catch (JsonMappingException e) {
				
				e.printStackTrace();
			} catch (IOException e) {
			
				e.printStackTrace();
			}
		 }
			 
		return "44444";
}
	
	public Dachat() {
	}

		
}


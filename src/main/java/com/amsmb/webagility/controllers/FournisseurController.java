
package com.amsmb.webagility.controllers;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.amsmb.webagility.dao.IFournisseurDao;
import com.amsmb.webagility.entities.Dachat;
import com.amsmb.webagility.entities.Fournisseur;
import com.amsmb.webagility.entities.Ldachat;
import com.amsmb.webagility.services.IDachatService;
import com.amsmb.webagility.services.IFournisseurService;
import com.amsmb.webagility.services.ILdachatService;
import com.lowagie.text.pdf.OutputStreamCounter;
import com.lowagie.text.pdf.codec.Base64.OutputStream;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignQuery;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.export.SimplePdfReportConfiguration;
import net.sf.jasperreports.view.JasperViewer;


@Controller
@RequestMapping(value = "/fournisseur")
public class FournisseurController {
	
	@Autowired
	private IFournisseurService fournisseurservice;
	

	
	@RequestMapping(value = "/")
	public String fournisseur(Model model) {
		
		List <Fournisseur> fournisseurs = fournisseurservice.selectAll();	
		if (fournisseurs == null)
		{
			fournisseurs = new ArrayList<Fournisseur>();
		}

		model.addAttribute("fournisseurs", fournisseurs);
		return "fournisseur/fournisseur";
	}
	
	@RequestMapping(value = "/nouveau", method = RequestMethod.GET)
	public String ajouterFournisseur(Model model) {
		Fournisseur fournisseur = new Fournisseur();
		model.addAttribute("fournisseur", fournisseur);
		return "fournisseur/ajouterFournisseur";
	}

	
	
}

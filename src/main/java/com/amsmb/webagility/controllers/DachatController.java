
package com.amsmb.webagility.controllers;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.amsmb.webagility.entities.Dachat;
import com.amsmb.webagility.entities.Fournisseur;
import com.amsmb.webagility.entities.Ldachat;
import com.amsmb.webagility.model.ModelDemandeAchat;
import com.amsmb.webagility.services.IDachatService;
import com.amsmb.webagility.services.IFournisseurService;
import com.amsmb.webagility.services.ILdachatService;
import com.lowagie.text.pdf.OutputStreamCounter;
import com.lowagie.text.pdf.codec.Base64.OutputStream;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignQuery;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.export.SimplePdfReportConfiguration;
import net.sf.jasperreports.view.JasperViewer;

@Controller
@RequestMapping(value = "/dachat")
public class DachatController {

	@Autowired
	private IDachatService dachatService;

	@Autowired
	private ILdachatService ldachatservice;
	
	@Autowired
	private IFournisseurService fournisseurservice;
	
	@Autowired
	private ModelDemandeAchat modeldemandeachat;

	@RequestMapping(value = "/")
	public String dachat(Model model) {

		List<Dachat> dachats = dachatService.selectAll();
		if (dachats == null) {
			dachats = new ArrayList<Dachat>();
		} else {
			for (Dachat dachat : dachats) {
				List<Ldachat> ldachats = ldachatservice.getByIdDachat(dachat.getIdDachat());
				dachat.setLdachats(ldachats);
			}
		}

		model.addAttribute("dachats", dachats);
		return "dachat/dachat";
	}

	@RequestMapping(value = "/nouveau", method = RequestMethod.GET)
	public String ajouterDachat(Model model) {

		List<Fournisseur> fournisseurs = fournisseurservice.selectAll();
		if (fournisseurs == null )
		{
			fournisseurs = new ArrayList<Fournisseur>();
		}
		Dachat dachat = new Dachat();
		model.addAttribute("dachat", dachat);
		model.addAttribute("fournisseurs", fournisseurs);
		return "dachat/ajouterDachat";
	}

	@RequestMapping(value = "/nouveau", method = RequestMethod.POST)
	public String enregistrerDachat(Model model, Dachat dachat) {

		if (dachat.getIdDachat() != null) {
			dachatService.update(dachat);
		} else

		{
			dachatService.save(dachat);
		}

		return "redirect:/dachat/";
	}

	@RequestMapping(value = "/modifier/{idDachat}")
	public String modifierDachat(Model model, @PathVariable Long idDachat) {
		Dachat dachat = dachatService.getById(idDachat);
		if (idDachat != null) {
			model.addAttribute("dachat", dachat);
		}

		return "dachat/ajouterDachat";

	}

	@RequestMapping(value = "/supprimer/{idDachat}")
	public String supprimerDachat(Model model, @PathVariable Long idDachat) {

		if (idDachat != null) {

			dachatService.remove(idDachat);
		}
		return "redirect:/dachat/";
	}

	@RequestMapping(value = "/print/{idDachat}", method = RequestMethod.GET)
	public String printDachat(HttpServletResponse response, @PathVariable Long idDachat) throws IOException {
		response.setContentType("text/html");
		try {

			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/webagility", "root", "");
			InputStream jrxmlInput = this.getClass().getResourceAsStream("/report/Dachat.jrxml");
			JasperDesign jdesign = JRXmlLoader.load(jrxmlInput);
			String query = "SELECT * FROM ( SELECT * FROM Dachat )A LEFT JOIN ( SELECT * FROM Ldachat  )B ON A.IdDachat = B.IdDachat";
			JRDesignQuery updateQuery = new JRDesignQuery();
			updateQuery.setText(query);

			jdesign.setQuery(updateQuery);

			JasperReport jreport = JasperCompileManager.compileReport(jdesign);
			JasperPrint jprint = JasperFillManager.fillReport(jreport, null, con);

			JRPdfExporter pdfExporter = new JRPdfExporter();
			pdfExporter.setExporterInput(new SimpleExporterInput(jprint));
			ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream();
			pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
			pdfExporter.exportReport();

			response.setContentType("application/pdf");
			response.setHeader("Content-Lenght", String.valueOf(pdfReportStream.size()));
			response.addHeader("Content-Disposition", "inline; filename=jasper.pdf;");

			ServletOutputStream responseOutputStream = response.getOutputStream();
			responseOutputStream.write(pdfReportStream.toByteArray());
			responseOutputStream.close();
			pdfReportStream.close();

			// JasperViewer.viewReport(jprint);
			/*
			 * JRPdfExporter exporter = new JRPdfExporter(); exporter.setExporterInput(new
			 * SimpleExporterInput(jprint)); exporter.setExporterOutput( new
			 * SimpleOutputStreamExporterOutput(
			 * "C:/Users/A-HAMADI/Desktop/employeeReport.pdf"));
			 * SimplePdfReportConfiguration reportConfig = new
			 * SimplePdfReportConfiguration(); reportConfig.setSizePageToContent(true);
			 * reportConfig.setForceLineBreakPolicy(false); SimplePdfExporterConfiguration
			 * exportConfig = new SimplePdfExporterConfiguration();
			 * exportConfig.setMetadataAuthor("baeldung"); exportConfig.setEncrypted(true);
			 * exportConfig.setAllowedPermissionsHint("PRINTING");
			 * exporter.setConfiguration(reportConfig); exporter.exportReport();
			 */

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "redirect:/dachat/";

	}

	public void reportPrint() {
		try {

			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/webagility", "root", "");
			JasperDesign jdesign = JRXmlLoader.load("C:\\Users\\A-HAMADI\\Desktop\\Dachat.jrxml");
			String query = "SELECT * FROM ( SELECT * FROM Dachat )A LEFT JOIN ( SELECT * FROM Ldachat  )B ON A.IdDachat = B.IdDachat";
			JRDesignQuery updateQuery = new JRDesignQuery();
			updateQuery.setText(query);

			jdesign.setQuery(updateQuery);

			JasperReport jreport = JasperCompileManager.compileReport(jdesign);
			JasperPrint jprint = JasperFillManager.fillReport(jreport, null, con);

			JRPdfExporter pdfExporter = new JRPdfExporter();
			pdfExporter.setExporterInput(new SimpleExporterInput(jprint));
			ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream();
			pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));

			// JasperViewer.viewReport(jprint);
			/*
			 * JRPdfExporter exporter = new JRPdfExporter(); exporter.setExporterInput(new
			 * SimpleExporterInput(jprint)); exporter.setExporterOutput( new
			 * SimpleOutputStreamExporterOutput(
			 * "C:/Users/A-HAMADI/Desktop/employeeReport.pdf"));
			 * SimplePdfReportConfiguration reportConfig = new
			 * SimplePdfReportConfiguration(); reportConfig.setSizePageToContent(true);
			 * reportConfig.setForceLineBreakPolicy(false); SimplePdfExporterConfiguration
			 * exportConfig = new SimplePdfExporterConfiguration();
			 * exportConfig.setMetadataAuthor("baeldung"); exportConfig.setEncrypted(true);
			 * exportConfig.setAllowedPermissionsHint("PRINTING");
			 * exporter.setConfiguration(reportConfig); exporter.exportReport();
			 */

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
